'''

Thanks to the developers of LSP for all their
direct/indirect contributions to LiveSession
'''
import sublime
from .plugin.core.manager import session_manager
from .plugin.session import (
    LiveSessionCreateCommand,
    LiveSessionJoinCommand,
    LiveSessionLeaveCommand,
    LiveSessionInfo,
)
from .plugin.edit import (
    LiveSessionApplyDocumentEditCommand,
    LiveSessionSelectionClearCommand,
    LiveSessionSelectionAddCommand,
)
from .plugin.core.registry import (
    LiveSessionTextChangeListener,
    LiveSessionEventListener,
    LiveSessionViewEventListener,
)

from .plugin.ui import LiveSessionOpenSessionInfoCommand
from .plugin.core.settings import load_settings, unload_settings


def plugin_loaded():
    load_settings()


def plugin_unloaded():
    session_manager.delete_session()
    unload_settings()
