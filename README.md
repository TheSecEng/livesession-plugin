## LiveSession - Sublime Plugin

## Preview

[![](http://img.youtube.com/vi/o8qpDdvTquc/0.jpg)](http://www.youtube.com/watch?v=o8qpDdvTquc "")

### Status

🚨 ENCRYPTION: Is planned but not implemented. All code streams are in plain text

- [ ] Websocket optimization umask improvements required
  - See numpy or wsaccel for example. Requires advance work
- [ ] Investigate python crypto speeds

Sublime Text © plugin that provide code sharing sessions.

## KNOWN BUGS

- [x] 🐛 BUG: Sometimes when opening a file, the contents aren't included
- [ ] 🐛 BUG: Better scrolling to current clients position (Sublime API Limitation)

#### TODO

See Project [Development Board][dev_board]



[dev_board]: https://gitlab.com/TheSecEng/livesession/-/boards/1892282