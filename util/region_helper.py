import sublime

from ..plugin.core.typing import Dict, List
from .util import decrypt_content, encrypt_content


def get_indicator_location(
    view: sublime.View, region: sublime.Region, location: str = 'eol'
) -> sublime.Region:
    if location == 'eol':
        region = view.line(region)
    if region.a < region.b:
        return sublime.Region(region.b, region.b)
    return sublime.Region(region.a, region.a)


def encrypt_regions(
    regions: List[Dict], key: bytes = b'', iv: bytes = b'', encrypted: bool = False,
) -> (bytes, List[Dict]):
    region_list = []
    encode_iv = ''
    for region in regions:
        if encrypted:
            encode_iv, content = encrypt_content(key, iv, region.get('content', ''))
        else:
            content = region.get('content', '')
        region_list.append(
            {'a': region.get('a', 0), 'b': region.get('b', 0), 'content': content}
        )
    return (encode_iv, region_list)


def decrypt_regions(
    regions: List[Dict], key: bytes = b'', iv: str = '', decrypt: bool = False,
) -> List[Dict]:
    region_list = []
    for region in regions:
        if decrypt:
            content = decrypt_content(key, iv, region.get('content', ''))
        else:
            content = region.get('content', '')
        region_list.append(
            {'a': region.get('a', 0), 'b': region.get('b', 0), 'content': content}
        )
    return region_list
