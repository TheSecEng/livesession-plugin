import sublime

from ..plugin.core.typing import Any


def get_syntax_info(view: sublime.View) -> dict:
    return {
        'path': view.settings().get('syntax'),
        'scope': view.scope_name(1).split(' ')[0],
    }


def syntax_exists(syntax_path: str) -> bool:
    syntax_files = sublime.list_syntaxes()
    return any(syntax.path == syntax_path for syntax in syntax_files)


def syntax_from_scope(scope: str) -> Any:
    syntax_files = sublime.list_syntaxes()
    return next(
        (syntax.scope for syntax in syntax_files if syntax.scope == scope),
        'Packages/Text/Plain text.tmLanguage',
    )
