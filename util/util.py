from base64 import b64decode, b64encode

from ..dep.oscrypto.symmetric import aes_cbc_pkcs7_decrypt, aes_cbc_pkcs7_encrypt
from ..dep.oscrypto.util import rand_bytes


def new_iv(length: int = 16) -> bytes:
    return rand_bytes(length)


def encrypt_content(key: bytes, iv: bytes, raw: str) -> (str, str):
    iv, content = aes_cbc_pkcs7_encrypt(key, bytes(raw, 'UTF-8'), iv)
    return (
        b64encode(iv).decode(encoding='UTF-8'),
        b64encode(content).decode(encoding='UTF-8'),
    )


def decrypt_content(key: bytes, iv: str, encrypted: str) -> str:
    decoded_iv = b64decode(iv)
    decoded_encrypted = b64decode(encrypted)
    return aes_cbc_pkcs7_decrypt(key, decoded_encrypted, decoded_iv).decode(
        encoding='UTF-8'
    )
