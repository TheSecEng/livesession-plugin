import sublime

from .core.registry import LiveSessionTextCommand
from .core.payloads import new_client_info_payload


class LiveSessionCreateCommand(LiveSessionTextCommand):
    def run(self, _):
        if not self.manager.new_session(self.view.window()):
            sublime.message_dialog('failed to create a session')
        else:
            sublime.set_clipboard(
                f'{self.manager.session_id()}:{self.manager.session_passphrase()}:{self.manager.session_key()}'
            )
            sublime.message_dialog(
                '[LiveSession]'
                f'\nSession: {self.manager.session_id()}'
                f'\nPassphrase: {self.manager.session_passphrase()}'
                f'\nEncryption Key: {self.manager.session_key()}'
            )

    def is_visible(self):
        return not self.manager.session_exists()

    def is_enabled(self):
        return not self.manager.session_exists()


class LiveSessionJoinCommand(LiveSessionTextCommand):
    def run(self, _):
        self.session_id = ''
        self.passphrase = ''
        self.key = ''

        if self.manager.session_exists():
            return

        self.view.window().show_input_panel(
            'Session ID', '', self.get_session_id, None, None
        )

    def get_session_id(self, text):
        session_info = text.strip(' ').split(':')
        self.session_id = session_info[0]
        if len(session_info) == 3:
            self.passphrase = session_info[1]
            self.key = session_info[2]
            self._join_session()
        elif len(session_info) == 2:
            self.passphrase = session_info[1]
            self._join_session()
        else:
            self.view.window().show_input_panel(
                'Session Passphrase', '', self.get_passphrase_id, None, None
            )

    def get_passphrase_id(self, text):
        self.passphrase = text.strip(' ')
        self._join_session()

    def _join_session(self):
        sublime.run_command('new_window')
        window = sublime.active_window()
        if not self.manager.join_session(
            window=window,
            session_id=self.session_id,
            passphrase=self.passphrase,
            encryption_key=self.key,
        ):
            sublime.message_dialog(
                f'[LiveSession]: failed to join session ({self.session_id})'
                f' with passphrase ({self.passphrase})'
            )
        else:
            client_info = new_client_info_payload(
                uid=self.manager.session_id(),
                passphrase=self.manager.session_passphrase(),
                host=self.manager.session_host(),
                sender=self.manager.me(),
            )
            self.manager.send_payload(client_info)
            sublime.message_dialog(
                f'[LiveSession]'
                f'\nHost: {self.manager.session_host().display_name}'
                f'\nSession: {self.manager.session_id()}'
                f'\nPassphrase: {self.manager.session_passphrase()}'
            )

    def is_visible(self):
        return not self.manager.session_exists()

    def is_enabled(self):
        return not self.manager.session_exists()


class LiveSessionLeaveCommand(LiveSessionTextCommand):
    def run(self, edit):
        response = sublime.yes_no_cancel_dialog(
            '[LiveSession]'
            f'\nHost: {self.manager.session_host().display_name}'
            '\n\nAre you sure you want to leave the session?'
        )
        if response == sublime.DIALOG_YES:
            self.manager.delete_session()


class LiveSessionInfo(LiveSessionTextCommand):
    def run(self, edit):
        window = self.view.window()
        window.show_quick_panel(
            self.manager.get_session_info(), lambda index: self._on_done(index),
        )

    def _on_done(self, index):
        if index == -1:
            return
        elif index == 0:
            session_info = (
                f'{self.manager.session_id()}:{self.manager.session_passphrase()}'
            )
            if self.manager.session_key():
                session_info = f'{session_info}:{self.manager.session_key()}'
            sublime.set_clipboard(session_info)
        elif index == 1:
            self.manager.list_active_clients()
