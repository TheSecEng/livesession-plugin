from .settings import settings


def info(msg: str) -> None:
    if not settings.logging:
        return

    if settings.logging_level == 'info' or settings.logging_level == 'debug':
        printf(msg)


def debug(msg: str) -> None:
    if not settings.logging:
        return

    if settings.logging_level == 'debug':
        printf(msg)


def printf(msg: str, prefix: str = 'LiveSession') -> None:
    print(f'{prefix}: {msg}')
