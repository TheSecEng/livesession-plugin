import re
import os
import sys

import sublime

# Strip potentialy bad chars from DisplayName
# Also occurs on Server-side
display_name_re = re.compile(r'[^a-zA-Z0-9\_\-\.]+')


class Settings(object):
    def __init__(self) -> None:
        # Debug Settings
        self.logging = False
        self.logging_level = 'info'
        # Server Settings
        self.host = ''  # type: str
        self.port = 443  # type: int
        self.tls = False
        self.encryption = True
        # User Settings
        self.display_name = str()
        self.user_indicator = str()
        self.user_indicator_location = str()
        self.tag_variables = []
        self.tag_css = ''
        # Workspace Settings
        self.gitignore = True
        self.lsignore = False
        self.display_active_clients = True
        self.cache_path = ''


_settings_obj = None
settings = Settings()


def load_settings() -> None:
    global _settings_obj
    settings_obj = sublime.load_settings('LiveSession.sublime-settings')
    _settings_obj = settings_obj
    update_settings(settings, settings_obj)
    settings_obj.add_on_change(
        '_on_updated_settings', lambda: update_settings(settings, settings_obj)
    )


def unload_settings() -> None:
    if _settings_obj:
        _settings_obj.clear_on_change('_on_updated_settings')


def get_boolean_setting(
    settings_obj: sublime.Settings, key: str, default: bool
) -> bool:
    val = settings_obj.get(key)
    if isinstance(val, bool):
        return val
    else:
        return default


def get_int_setting(settings_obj: sublime.Settings, key: str, default: int) -> int:
    val = settings_obj.get(key)
    if isinstance(val, int):
        return val
    else:
        return default


def get_dictionary_setting(
    settings_obj: sublime.Settings, key: str, default: dict
) -> dict:
    val = settings_obj.get(key)
    if isinstance(val, dict):
        return val
    else:
        return default


def get_list_setting(settings_obj: sublime.Settings, key: str, default: list) -> list:
    val = settings_obj.get(key)
    if isinstance(val, list):
        return val
    else:
        return default


def get_str_setting(settings_obj: sublime.Settings, key: str, default: str) -> str:
    val = settings_obj.get(key)
    if isinstance(val, str):
        return val
    else:
        return default


def get_dict_setting(settings_obj: sublime.Settings, key: str, default: dict) -> dict:
    val = settings_obj.get(key)
    if isinstance(val, dict):
        return val
    else:
        return default


def update_settings(settings: Settings, settings_obj: sublime.Settings) -> None:
    # Debug Settings
    settings.logging = get_boolean_setting(settings_obj, 'logging', False)
    settings.logging_level = get_str_setting(settings_obj, 'logging.level', 'info')
    # Server Settings
    settings.host = get_str_setting(settings_obj, 'server.host', 'localhost')
    settings.port = get_int_setting(settings_obj, 'server.port', 443)
    settings.tls = get_boolean_setting(settings_obj, 'server.tls', True)
    settings.encryption = get_boolean_setting(settings_obj, 'encryption', False)
    # User Settings
    settings.display_name = get_str_setting(settings_obj, 'user.display_name', '')
    settings.display_name = display_name_re.sub(
        '', get_str_setting(settings_obj, 'user.display_name', '')
    )
    settings.user_indicator = get_boolean_setting(settings_obj, 'user.indicator', True)
    settings.user_indicator_location = get_str_setting(
        settings_obj, 'user.indicator.location', 'eol'
    )
    settings.tag_variables = get_list_setting(
        settings_obj,
        'user.indicator.styles.variables',
        ['red', 'orange', 'yellow', 'green', 'cyan', 'blue', 'purple', 'pink',],
    )
    settings.tag_css = get_str_setting(
        settings_obj,
        'user.indicator.styles.css',
        '.user-indicator {display: block;font-size: 0.75rem;border-radius: 0.25rem;height: 0.15rem;padding: 0.50rem;}.user-indicator .display_name {padding-top: -0.3rem;}.tag.red {background-color: #FF583A;color: black;}.tag.orange {background-color: #FF7C40;color: black;}.tag.yellow {background-color: #FFDD40;color: black;}.tag.green {background-color: #83FF40;color: black;}.tag.cyan {background-color: #47EAFF;color: black;}.tag.blue {background-color: #0AF;color: black;}.tag.purple {background-color: #BC40FF;color: white;}.tag.pink {background-color: #FF4083;color: black;}',
    )
    # Workspace Settings
    settings.gitignore = get_boolean_setting(settings_obj, 'workspace.gitignore', True)
    settings.lsignore = get_boolean_setting(settings_obj, 'workspace.lsignore', False)
    settings.display_active_clients = get_boolean_setting(
        settings_obj, 'workspace.display.active_clients', True
    )
    settings.cache_path = get_str_setting(
        settings_obj,
        'workspace.cache_path',
        os.path.join(sublime.cache_path(), 'LiveSession'),
    )

    if not settings.cache_path:
        settings.cache_path = os.path.join(sublime.cache_path(), 'LiveSession')

    if not _path_exists(settings.cache_path):
        _create_path(settings.cache_path)


def _path_exists(path: str) -> bool:
    return os.path.exists(path)


def _create_path(path: str) -> None:
    try:
        os.makedirs(path, exist_ok=True)
    except FileExistsError as ex:
        debug(ex)
        sys.exit(-1)
