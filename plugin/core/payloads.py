from urllib.parse import urlencode
from .typing import Any, Dict, List
from .events import (
    TYPE_BLOCK_CLIENT,
    TYPE_CLIENT_INFO,
    TYPE_EXIT,
    TYPE_FILE_CLOSED,
    TYPE_FILE_OPENED,
    TYPE_INCREMENTAL,
    TYPE_SELECTION_MODIFIED,
)
from .user import User


'''
{
    "timestamp": "0000000000",
    "type": 0,
    "uid": "DD6B8B6A-3738-47BD-99F7-8604EDCAEDBA",
    "session": {
        "uid": "89AF8AD0-DDA6-47AD-B73F-CF37B88F044B",
        "passphrase": "C3390AC9-B8E5-4395-ABD5-96BE0CAAFD1B",
        "encrypted": true,
        "host": {
            "uid": "CAF82C5B-50F2-4626-846A-7C3E167A4207",
            "session": "CBAFDBB2-F027-45F2-9ADF-01A31CE53A93",
            "display_name": "Duster",
            "is_host": false,
            "is_editor": false,
        }
        "clients": [{
            "uid": "CAF82C5B-50F2-4626-846A-7C3E167A4207",
            "session": "CBAFDBB2-F027-45F2-9ADF-01A31CE53A93",
            "display_name": "Duster",
            "is_host": false,
            "is_editor": false,
        },
        {
            "uid": "CAF82C5B-50F2-4626-846A-7C3E167A4207",
            "session": "CBAFDBB2-F027-45F2-9ADF-01A31CE53A93",
            "display_name": "Duster",
            "is_host": false,
            "is_editor": false,
        },
        {
            "uid": "CAF82C5B-50F2-4626-846A-7C3E167A4207",
            "session": "CBAFDBB2-F027-45F2-9ADF-01A31CE53A93",
            "display_name": "Duster",
            "is_host": false,
            "is_editor": false,
        }]
    },
    "sender": {
        "uid": "CAF82C5B-50F2-4626-846A-7C3E167A4207",
        "session": "CBAFDBB2-F027-45F2-9ADF-01A31CE53A93",
        "display_name": "Duster",
        "is_host": false,
        "is_editor": false,
    },
    "body": json.RawMessage (Go)
}
'''


def create_session_request(
    host: str, port: int, tls: bool, display_name: str = '', encrypted: bool = False,
) -> str:
    params = {'action': 'create', 'encrypted': encrypted}
    if display_name:
        params['display_name'] = display_name
    return new_request(host=host, port=port, params=params, tls=tls)


def join_session_request(
    host: str,
    port: int,
    tls: bool,
    session_id: str,
    passphrase: str,
    display_name: str = '',
    encrypted: bool = False,
) -> str:
    params = {
        'action': 'join',
        'session': session_id,
        'passphrase': passphrase,
        'encrypted': encrypted,
    }
    if display_name:
        params['display_name'] = display_name
    return new_request(host=host, port=port, params=params, tls=tls)


def new_request(
    params: Dict, host: str = 'localhost', port: int = 8080, tls: bool = True
) -> str:
    return f'''{'wss' if tls else 'ws'}://{host}:{port}/ws?{urlencode(params)}'''


def gen_session_info(
    uid: str, passphrase: str, host: User, encrypted: bool = False,
) -> Dict:
    return {
        'uid': uid,
        'host': {
            'uid': host.uid,
            'display_name': host.display_name,
            'is_host': host.host,
            'is_editor': host.editor,
        },
        'passphrase': passphrase,
        'encrypted': encrypted,
    }


def gen_sender_info(sender: User) -> Dict:
    return {
        'uid': sender.uid,
        'display_name': sender.display_name,
        'is_host': sender.host,
        'is_editor': sender.editor,
    }


def gen_client_info(client: User) -> Dict:
    return {
        'uid': client.uid,
        'display_name': client.display_name,
        'is_host': client.host,
        'is_editor': client.editor,
    }


def gen_region_info(
    file_id: str,
    file_name: str,
    iv: str = '',
    syntax: Dict = {},
    viewport: Dict = {},
    regions: List = [],
):
    payload = {'file_id': file_id, 'file_name': file_name}
    if iv:
        payload['iv'] = iv
    if syntax:
        payload['syntax'] = syntax
    if viewport:
        payload['viewport'] = viewport
    if regions:
        payload['regions'] = regions
    return payload


def new_client_kick_payload(
    uid: str, passphrase: str, host: User, sender: User, client: User
) -> Dict:
    session = gen_session_info(uid, passphrase, host)
    sender = gen_sender_info(sender)
    client = gen_client_info(client)
    return _new_payload(
        TYPE_BLOCK_CLIENT, session=session, sender=sender, client=client
    )


def new_client_info_payload(
    uid: str, passphrase: str, host: User, sender: User
) -> Dict:
    session = gen_session_info(uid, passphrase, host)
    sender = gen_sender_info(sender)
    return _new_payload(TYPE_CLIENT_INFO, session=session, sender=sender)


def new_exit_payload(uid: str, passphrase: str, host: User, sender: User) -> Dict:
    session = gen_session_info(uid, passphrase, host)
    sender = gen_sender_info(sender)
    return _new_payload(TYPE_EXIT, session=session, sender=sender)


def new_open_payload(
    uid: str,
    passphrase: str,
    encrypted: bool,
    host: User,
    sender: User,
    regions: List[Dict],
) -> Dict:
    session = gen_session_info(uid, passphrase, host, encrypted)
    sender = gen_sender_info(sender)
    for region in regions:
        region['type'] = TYPE_FILE_OPENED
    return _new_payload(
        TYPE_FILE_OPENED, session=session, sender=sender, content=regions
    )


def new_closed_payload(
    uid: str, passphrase: str, host: User, sender: User, regions: List[Dict]
) -> Dict:
    session = gen_session_info(uid, passphrase, host)
    sender = gen_sender_info(sender)
    for region in regions:
        region['type'] = TYPE_FILE_CLOSED
    return _new_payload(
        TYPE_FILE_CLOSED, session=session, sender=sender, content=regions
    )


def new_incremental_payload(
    uid: str,
    passphrase: str,
    encrypted: bool,
    host: User,
    sender: User,
    regions: List[Dict],
) -> Dict:
    session = gen_session_info(uid, passphrase, host, encrypted)
    sender = gen_sender_info(sender)
    for region in regions:
        region['type'] = TYPE_INCREMENTAL
    return _new_payload(
        TYPE_INCREMENTAL, session=session, sender=sender, content=regions
    )


def new_selection_payload(
    uid: str, passphrase: str, host: User, sender: User, regions: List[Dict]
) -> Dict:
    session = gen_session_info(uid, passphrase, host)
    sender = gen_sender_info(sender)
    for region in regions:
        region['type'] = TYPE_SELECTION_MODIFIED
    return _new_payload(
        TYPE_SELECTION_MODIFIED, session=session, sender=sender, content=regions
    )


def _new_payload(
    t: int, session: Dict, sender: Dict, content: List[Dict] = [{}]
) -> Dict:
    payload = {'type': t, 'session': session, 'sender': sender}

    if content:
        payload['body'] = content
    return payload
