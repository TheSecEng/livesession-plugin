import os

import sublime

from ...dep.gitignore.parser import parse_gitignore
from ...util.view_helper import new_uuid, normalize_path
from .logger import info
from .settings import settings
from .typing import Any, Callable, Dict, List, Optional


def ignore_matcher(folders: List[str], match_type: str) -> Dict[str, Callable]:
    ignore = {}
    for folder in folders:
        if os.path.exists(os.path.join(folder, match_type)):
            ignore[folder] = parse_gitignore(os.path.join(folder, match_type))
    return ignore


class Workspace:
    def __init__(self, window: sublime.Window):
        self._cache_project = False
        self._cache_path = ''
        self._window = window  # type: sublime.Window
        self.folders = self._window.folders()  # type: List[str]
        self._views = {}  # type: Dict[str, Dict[str, Any]]
        self.phantom_sets = {}  # type:  Dict[str, sublime.PhantomSet]
        self.gitignore = ignore_matcher(
            self.folders, '.gitignore'
        )  # type: Dict[str, Callable]
        self.lsignore = ignore_matcher(
            self.folders, '.lsignore'
        )  # type: Dict[str, Callable]

    def window(self) -> sublime.Window:
        return self._window

    def view(self, uid) -> Optional[sublime.View]:
        return next(
            (
                view
                for view in self._window.views()
                if view.settings().get('livesession.view.uid') == uid
            ),
            None,
        )

    def is_workspace(self, window_id: int) -> bool:
        return self._window.id() == window_id

    def close(self) -> None:
        self._window.run_command('close_window')

    def focus_workspace(self) -> None:
        self._window.bring_to_front()

    def focus_view(self, view: sublime.View) -> None:
        if view.settings().get('livesession.view.uid'):
            self._window.focus_view(view)

    def update_folders(self) -> List[str]:
        self.folders = self._window.folders()
        return self.folders

    def update_views(self) -> Dict[str, sublime.View]:
        for view in self._window.views():
            uid = view.settings().get('livesession.view.uid')
            if not uid:
                uid = new_uuid()
            file_name = view.file_name() or view.name() or 'untitled'
            self._views[uid] = {
                'phantoms': sublime.PhantomSet(view, f'user_indicator_{uid}'),
                'relative_name': _workspace_relative_file_name(self.folders, file_name),
            }

    def new_view(
        self, uid: str, file_name: str, read_only: bool, scratch: bool, syntax: str,
    ) -> Dict[str, Any]:
        view = self._window.new_file()
        view.settings().set('livesession.view.uid', uid)
        view.set_scratch(scratch)
        view.set_syntax_file(syntax)
        view.set_name(file_name)
        view.set_read_only(read_only)
        view_ref = {
            'phantoms': sublime.PhantomSet(view, f'user_indicator_{uid}'),
            'relative_name': _workspace_relative_file_name(self.folders, file_name),
        }
        self._views[uid] = view_ref
        return view_ref

    def view_ref(self, uid) -> Dict[str, Any]:
        if uid in self._views:
            return self._views.get(uid)
        return None

    def add_view(self, view: sublime.View, uid: str) -> Optional[sublime.View]:
        if settings.gitignore and self.match_gitignore(view):
            info('[.gitignore]: ignoring file due to matching pattern')
            return None

        if settings.lsignore and self.match_lsignore(view):
            info('[.lsignore]: ignoring file due to matching pattern')
            return None

        if uid not in self._views:
            view.settings().set('livesession.view.uid', uid)
            file_name = view.file_name() or view.name() or 'untitled'
            self._views[uid] = {
                'phantoms': sublime.PhantomSet(view, f'user_indicator_{uid}'),
                'relative_name': _workspace_relative_file_name(self.folders, file_name),
            }
        return self._views[uid]

    def remove_view(self, uid: str) -> bool:
        if self.view_ref(uid):
            self._views.pop(uid)
        view = self.view(uid)
        if view:
            view.close()
        return True

    def view_exists(self, view) -> bool:
        uuid = view.settings().get('livesession.view.uid')
        if not uuid or uuid not in self._views:
            return False
        return True

    def clear_phantom_sets(self, uid: str = '') -> None:
        if uid and self.view_ref(uid):
            phantoms = self.view_ref(uid).get('phantoms')
            phantoms.update([])
        else:
            for view in self._views:
                view['phantoms'].update([])

    def match_gitignore(self, view: sublime.View) -> bool:
        if not view.file_name():
            return
        if not any(view.file_name().startswith(path) for path in self.folders):
            return
        return any(matcher(view.file_name()) for _, matcher in self.gitignore.items())

    def match_lsignore(self, view: sublime.View) -> bool:
        if not view.file_name():
            return
        if not any(view.file_name().startswith(path) for path in self.folders):
            return
        return any(matcher(view.file_name()) for _, matcher in self.lsignore.items())


def _workspace_relative_file_name(folders: List, file_name: str) -> str:
    for folder in folders:
        file_name = normalize_path(file_name)
        norm_folder = normalize_path(folder)
        if file_name.startswith(norm_folder):
            base = norm_folder.split('/')[-1]
            file_name = file_name.replace(norm_folder, '')
            if base[-1] != '/' and file_name[0] != '/':
                base = f'{base}/'
            return f'{base}{file_name}'
