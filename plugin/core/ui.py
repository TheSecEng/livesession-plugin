import sublime
from weakref import ref

DEFAULT_LAYOUT = {
    "cols": [0.0, 0.75, 1.0],
    "rows": [0.0, 0.60, 1.0],
    "cells": [[0, 0, 1, 2], [1, 0, 2, 1], [1, 1, 2, 2]],
}

TEMPLATE_INFO = '''
<html>
   <style>
      html {{
      background-color: color(var(--background) s(65%));
      }}
      body {{
            margin: 0.5rem;
            }}
      h2 {{
            color: var(--accent);
            }}
      h5 {{
            color: var(--bluish);
            }}
      .copy-all, .copy {{
            font-size: 8;
            }}
      div.container pre a {{
            text-decoration: none;
            color: var(--redish);
            font-weight:  bold;
            }}
      div.container  {{
            border: 1px solid color(var(--foreground) a(0.1));
            border-radius: 0.25rem;
            font-size: 0.9rem;
            }}
      div.container pre {{
            padding: 1rem;
            }}
   </style>
   <body>
      {}
      <h3>Active Participants</h3>
      <div class="container">
         <pre>
            {}
        </pre>
      </div>
      <br>
      <h3>Editors</h3>
      <div class="container">
         <pre>
            {}
        </pre>
      </div>
      <br>
      <h3>Blocked Participants</h3>
      <div class="container">
        <pre>
            {}
        </pre>
      </div>
   </body>
</html>'''

SESSION_INFO_TEMPLATE = '''<h2>Session Info</h2>
      <a class="copy-all">(copy all)</a>
      <br>
      <h5>UID</h5>
      <div style="display: inline; font-size:1rem" >{} <a class="copy">(copy)</a></div>
      <br>
      <h5>Passphrase</h5>
      <div style="display: inline; font-size:1rem" >{} <a class="copy">(copy)</a></div>
      <br>
      <h5>Encrypted</h5>
      <div style="display: inline; font-size:1rem" >{}</div>
      <br>
      <br>'''

CLIENT_TEMPLATE = '''<a href='subl:kick_client {"args": {"client": "{}"}}'>x</a> - {}'''


class UIManager:
    def __init__(self, window: sublime.Window, session) -> None:
        self._session = session
        self._window = window
        self._html_sheet = None

    def setup_layout(self):
        if self._window:
            self._window.set_layout(DEFAULT_LAYOUT)

    def open_or_update_ui(self):
        client_html = '<br>'.join(
            CLIENT_TEMPLATE.format(client.uid, client.display_name)
            for client in self._session.clients
        )

        editor_client_html = '<br>'.join(
            CLIENT_TEMPLATE.format(client.uid, client.display_name)
            for client in self._session.blocked_clients
            if client.editor
        )

        blocked_client_html = '<br>'.join(
            CLIENT_TEMPLATE.format(client.uid, client.display_name)
            for client in self._session.blocked_clients
        )
        session_html = SESSION_INFO_TEMPLATE.format(
            self._session.uid, self._session.passphrase, self._session.encrypted
        )
        final_html = TEMPLATE_INFO.format(
            session_html, client_html, editor_client_html, blocked_client_html
        )

        self.setup_layout()
        self._window.focus_group(1)
        if not self._html_sheet or self._html_sheet not in self._window.sheets():
            self._html_sheet = self._window.new_html_sheet(
                "Session Info", final_html, 1
            )
            self._window.focus_group(0)
            return
        self._html_sheet.set_contents(final_html)
        self._window.focus_group(0)
