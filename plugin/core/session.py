import sublime

from ...util.region_helper import decrypt_regions, get_indicator_location
from ...util.syntax import syntax_exists, syntax_from_scope
from ...util.view_helper import get_uid, update_active_client_status
from .connection import WebsocketManager
from .events import (
    TYPE_UNKNOWN,
    TYPE_SUCCESS,
    TYPE_FAILED,
    TYPE_CREATE_SUCCESS,
    TYPE_CREATE_FAILED,
    TYPE_JOIN_SUCCESS,
    TYPE_JOIN_FAILED,
    TYPE_REQUEST_FULL_BUFFER,
    TYPE_REQUEST_WRITE,
    TYPE_APPROVE_WRITE,
    TYPE_DENY_WRITE,
    TYPE_REVOKE_WRITE,
    TYPE_FILE_OPENED,
    TYPE_FILE_CLOSED,
    TYPE_FILE_FOCUSED,
    TYPE_REQUEST_WORKSPACE,
    TYPE_WORKSPACE,
    TYPE_FULL_BUFFER,
    TYPE_INCREMENTAL,
    TYPE_SELECTION_MODIFIED,
    TYPE_BLOCK_CLIENT,
    TYPE_UNBLOCK_CLIENT,
    TYPE_GET_CLIENTS,
    TYPE_FOCUS_SESSION,
    TYPE_FOCUS_VIEW,
    TYPE_ENFORCE_FOLLOW,
    TYPE_UPDATE_CLIENT_INFO,
    TYPE_CLIENT_INFO,
    TYPE_CLIENT_LEFT,
    TYPE_EXIT,
)
from .logger import debug, info
from .settings import settings
from .typing import Callable, Dict, List, Tuple
from .user import User
from .workspace import Workspace
from .ui import UIManager

session = None


class LiveSession(object):
    def __init__(
        self,
        window: sublime.Window,
        endpoint: str,
        encrypted: bool,
        callback_close: Callable,
    ) -> None:
        self.ui_manager = UIManager(window, self)
        self.workspace = Workspace(window)
        self.key = b''
        self.encrypted = encrypted
        self.uid = ''
        self.passphrase = ''
        self.host = None  # type: User
        self.me = None  # type: User
        self.following = None  # type: User
        self.clients = []  # type: List[User]
        self.blocked_clients = []  # type: List[User]
        self.endpoint = endpoint
        self.wsm = None  # type: WebsocketManager
        self.callback_close = callback_close
        self.processor = {
            TYPE_UNKNOWN: self._type_unknown,
            TYPE_SUCCESS: self._type_success,
            TYPE_FAILED: self._type_failed,
            TYPE_CREATE_SUCCESS: self._type_create_success,
            TYPE_CREATE_FAILED: self._type_create_failed,
            TYPE_JOIN_SUCCESS: self._type_join_success,
            TYPE_JOIN_FAILED: self._type_join_failed,
            TYPE_REQUEST_FULL_BUFFER: self._type_request_full_buffer,
            TYPE_REQUEST_WRITE: self._type_request_write,
            TYPE_APPROVE_WRITE: self._type_approve_write,
            TYPE_FILE_OPENED: self._type_file_opened,
            TYPE_FILE_CLOSED: self._type_file_closed,
            TYPE_FILE_FOCUSED: self._type_file_focused,
            TYPE_REQUEST_WORKSPACE: self._type_request_workspace,
            TYPE_WORKSPACE: self._type_workspace,
            TYPE_FULL_BUFFER: self._type_full_buffer,
            TYPE_INCREMENTAL: self._type_incremental,
            TYPE_SELECTION_MODIFIED: self._type_selection_modified,
            TYPE_EXIT: self._type_exit,
            TYPE_CLIENT_INFO: self._type_client_info,
            TYPE_CLIENT_LEFT: self._type_client_left,
        }

    def run(self):
        self.wsm = WebsocketManager(
            endpoint=self.endpoint,
            callback_processor=self._on_received_payload,
            callback_connection=self.callback_close,
        )
        return self.wsm.start()

    def stop(self):
        if self.wsm:
            self.wsm.stop()

        if not self._is_host():
            self.workspace.close()
        self.workspace = None

    def _is_host(self) -> bool:
        return bool(self.host.uid == self.me.uid) if self.host and self.me else False

    def _has_write_permissions(self) -> bool:
        if not self.me:
            return bool(self.me)
        return self.me.host or self.me.editor

    def _on_received_payload(self, payload: dict) -> None:
        payload_type = payload.get('type', None)
        processor = self.processor.get(payload_type)
        if processor:
            processor(payload)

    def update_user_indicator_for_view(
        self, view: sublime.View, user_indicator: str, regions: List[Tuple[int, int]]
    ) -> None:
        uid = get_uid(view)
        if not settings.user_indicator or not uid:
            return

        view_ref = self.workspace.view_ref(uid)
        phantoms = [
            sublime.Phantom(
                get_indicator_location(
                    view, sublime.Region(*region), settings.user_indicator_location
                ),
                user_indicator,
                sublime.LAYOUT_INLINE,
            )
            for region in regions
        ]
        view_ref['phantoms'].update(phantoms)

    def focus_view(self, view: sublime.View):
        self.workspace.focus_view(view)

    def focus_session(self) -> None:
        self.workspace.focus_workspace()

    def _type_unknown(self, payload: dict) -> None:
        info(f'unknown payload {payload}')

    def _type_failed(self, payload: dict) -> None:
        info(f'failed payload {payload}')

    def _type_success(self, payload: dict) -> None:
        info(f'success payload {payload}')

    def _type_create_success(self, payload: dict) -> None:
        info('_type_create_success')
        debug(f'_type_create_success {payload}')
        session = payload.get('session', None)
        client = payload.get('sender', None)
        if not session or not client:
            return

        self.uid = session.get('uid')
        self.passphrase = session.get('passphrase')
        self.encrypted = session.get('encrypted')
        self.host = User(
            uid=session.get('host').get('uid'),
            display_name=session.get('host').get('display_name'),
            host=session.get('host').get('is_host'),
            editor=session.get('host').get('is_editor'),
        )
        self.me = User(
            uid=client.get('uid'),
            display_name=client.get('display_name'),
            host=client.get('is_host'),
            editor=client.get('is_editor'),
        )
        self.wsm.toggle_create_join_response(True)

    def _type_create_failed(self, payload: dict) -> None:
        sublime.message_dialog(
            f'''[LiveSession] failed to create session\n{payload.get('message')}'''
        )

    def _type_join_success(self, payload: dict) -> None:
        info('_type_join_success')
        debug(f'_type_join_success {payload}')
        session = payload.get('session', None)
        client = payload.get('sender', None)
        clients = session.get('clients', [])

        if not session or not client:
            return

        if session.get('encrypted', False) and not self.key:
            info('missing encryption key')
            self.callback_close()

        self.uid = session.get('uid')
        self.encrypted = session.get('encrypted', False)
        self.passphrase = session.get('passphrase')
        self.host = User(
            uid=session.get('host').get('uid'),
            display_name=session.get('host').get('display_name'),
            host=session.get('host').get('is_host'),
            editor=session.get('host').get('is_editor'),
        )
        self.me = User(
            uid=client.get('uid'),
            display_name=client.get('display_name'),
            host=client.get('is_host'),
            editor=client.get('is_editor'),
        )
        self.following = self.host
        self.clients.append(self.host)
        for c in clients:
            self.clients.append(
                User(
                    uid=client.get('uid'),
                    display_name=client.get('display_name'),
                    host=client.get('is_host'),
                    editor=client.get('is_editor'),
                )
            )

        self.wsm.toggle_create_join_response(True)

    def _type_join_failed(self, payload: dict) -> None:
        info(f'failed to join session: {payload}')

    def _type_request_full_buffer(self, payload: dict) -> None:
        pass

    def _type_request_write(self, payload: dict) -> None:
        info('_type_request_write')
        debug(f'_type_request_write {payload}')
        sender = payload.get('sender')
        sublime.message_dialog(
            f'''Client: {sender['display_name']} requests write permissions'''
        )

    def _type_approve_write(self, payload: dict) -> None:
        pass

    def _type_file_opened(self, payload: dict) -> None:
        info('_type_file_opened')
        debug(f'_type_file_opened {payload}')
        sender_info = payload.get('sender', None)
        sender = None
        if sender_info:
            for c in self.clients:
                if not c.uid == sender_info.get('uid'):
                    continue
                sender = c
                break

        payload_info = payload.get('body')
        if not payload_info:
            info('invalid payload')
            return

        for item in payload_info:
            iv = item.get('iv')
            file_name = item.get('file_name', 'unknown')
            file_id = item.get('file_id')
            region_info = item.get('regions')
            viewport = item.get('viewport')
            syntax = _get_syntax(item.get('syntax'))

            view_ref = self.workspace.view_ref(file_id)
            if not view_ref:
                view_ref = self.workspace.new_view(
                    file_id,
                    file_name,
                    not (self.me.host or self.me.editor),
                    True,
                    syntax,
                )

            view = self.workspace.view(file_id)
            if not view:
                return

            if self.following == sender:
                self.workspace.focus_view(view)

            _apply_view_edits_async(
                view,
                decrypt_regions(region_info, self.key, iv, self.encrypted),
                (viewport.get('a', 0), viewport.get('b', 0)),
                bool(self.following == sender),
            )

    def _type_file_closed(self, payload: dict) -> None:
        info('_type_file_closed')
        payload_info = payload.get('body')
        for item in payload_info:
            file_id = item.get('file_id')
            self.workspace.remove_view(file_id)

    def _type_file_focused(self, payload: dict) -> None:
        pass

    def _type_request_workspace(self, payload: Dict) -> None:
        if self.host != self.me:
            return
        return
        # self.workspace.generate_payload()

    def _type_workspace(self, payload: Dict) -> None:
        pass

    def _type_full_buffer(self, payload: dict) -> None:
        pass

    def _type_incremental(self, payload: dict) -> None:
        info('_type_incremental')
        debug(f'_type_incremental {payload}')
        sender_info = payload.get('sender', None)
        sender = None
        if sender_info:
            for c in self.clients:
                if not c.uid == sender_info.get('uid'):
                    continue
                sender = c
                break

        payload_info = payload.get('body')
        for item in payload_info:
            iv = item.get('iv', '')
            file_id = item.get('file_id')
            region_info = item.get('regions')
            viewport = item.get('viewport')
            syntax = _get_syntax(item.get('syntax'))

            view_ref = self.workspace.view_ref(file_id)
            if view_ref:

                view = self.workspace.view(file_id)
                if not view:
                    return

                view.set_syntax_file(syntax)
                _apply_view_edits_async(
                    view,
                    decrypt_regions(region_info, self.key, iv, self.encrypted),
                    (viewport.get('a', 0), viewport.get('b', 0)),
                    bool(self.following == sender),
                )
            else:
                self._type_file_opened(payload)

    def _type_selection_modified(self, payload: dict) -> None:
        info('_type_selection_modified')
        debug(f'_type_selection_modified {payload}')
        sender_info = payload.get('sender', None)
        sender = None
        if sender_info:
            for c in self.clients:
                if not c.uid == sender_info.get('uid'):
                    continue
                sender = c
                break

        payload_info = payload.get('body')
        for item in payload_info:
            file_id = item.get('file_id')
            region_info = item.get('regions')
            viewport = item.get('viewport')
            syntax = _get_syntax(item.get('syntax'))

            view_ref = self.workspace.view_ref(file_id)
            if not view_ref:
                return

            view = self.workspace.view(file_id)
            if not view:
                return

            regions = [(region['a'], region['b']) for region in region_info]
            view.set_syntax_file(syntax)
            _apply_view_selections_async(
                view,
                regions,
                (viewport.get('a', 0), viewport.get('b', 0)),
                self.host,
                bool(self.following == sender),
            )

    def _type_exit(self, payload: dict) -> None:
        self.callback_close()

    def _type_client_info(self, payload: dict):
        info('_type_client_info')
        debug(f'_type_client_info {payload}')
        client_info = payload.get('client', None)
        if not client_info:
            return None

        client = None
        for c in self.clients:
            if c.uid == client_info.get('uid'):
                client = c
                break

        if client:
            client.display_name = client_info.get('display_name', client.display_name)
        elif client_info.get('uid') != self.me.uid:
            client = User(
                uid=client_info.get('uid'),
                display_name=client_info.get('display_name'),
                host=client_info.get('is_host'),
                editor=client_info.get('is_editor'),
            )
            self.clients.append(client)

        if settings.display_active_clients:
            update_active_client_status(
                view=self.workspace.window().active_view(), count=len(self.clients),
            )

    def _type_client_left(self, payload: dict):
        info('_type_client_left')
        debug(f'_type_client_left {payload}')
        client_info = payload.get('client', None)
        if not client_info:
            return None

        for c in self.clients:
            if c.uid == client_info.get('uid'):
                self.clients.pop(self.clients.index(c))


def _apply_view_edits_async(
    view: sublime.View, changes: List[Dict], viewport: tuple, following: bool
) -> None:
    sublime.set_timeout_async(
        view.run_command(
            'live_session_apply_document_edit',
            {'changes': changes, 'viewport': viewport, 'following': following},
        )
    )


def _apply_view_selections_async(
    view: sublime.View,
    regions: List[Tuple],
    viewport: tuple,
    user: User,
    following: bool,
) -> None:
    if following:
        sublime.set_timeout_async(view.run_command('live_session_selection_clear'))
    sublime.set_timeout_async(
        view.run_command(
            'live_session_selection_add',
            {
                'regions': regions,
                'user_indicator': user.indicator(),
                'viewport': viewport,
                'following': following,
            },
        )
    )


def _get_syntax(syntax_info: dict) -> str:
    default_syntax = 'Packages/Text/Plain text.tmLanguage'

    if syntax_info.get('path') and syntax_exists(syntax_info.get('path')):
        return syntax_info.get('path')

    if syntax_info.get('scope'):
        return syntax_from_scope(syntax_info.get('scope'))
    return default_syntax
