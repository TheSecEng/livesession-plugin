from base64 import b64decode, b64encode

import sublime

from ...dep.oscrypto.util import rand_bytes
from ...util.view_helper import (
    is_transient_view,
    new_uuid,
)
from .logger import info
from .payloads import (
    create_session_request,
    join_session_request,
    new_client_kick_payload,
    new_exit_payload,
)
from .session import LiveSession
from .settings import settings
from .typing import Dict, List, Optional
from .user import User


class SessionManager:
    def __init__(self):
        self.session = None  # type: LiveSession
        self.window = None  # type: sublime.Window
        self.last_selection = {}  # type: Dict[str, sublime.Region]
        self.selection_set = {}  # type: Dict[str, List]

    def new_session(self, window: sublime.Window) -> bool:
        if self.session:
            info('already in a session')
            return False

        self.window = window
        endpoint = create_session_request(
            host=settings.host,
            port=settings.port,
            tls=settings.tls,
            display_name=settings.display_name,
            encrypted=settings.encryption,
        )
        session = LiveSession(
            window=self.window,
            endpoint=endpoint,
            encrypted=settings.encryption,
            callback_close=self.delete_session,
        )
        if session.run():
            self.session = session
            self.session.key = rand_bytes(32)
        return bool(self.session)

    def join_session(
        self,
        window: sublime.Window,
        session_id: str,
        passphrase: str,
        encryption_key: str = '',
    ) -> bool:
        if self.session:
            info('already in a session')
            return False

        self.window = window
        endpoint = join_session_request(
            host=settings.host,
            port=settings.port,
            tls=settings.tls,
            session_id=session_id,
            passphrase=passphrase,
            display_name=settings.display_name,
            encrypted=bool(encryption_key),
        )
        self.session = LiveSession(
            window=self.window,
            endpoint=endpoint,
            encrypted=bool(encryption_key),
            callback_close=self.delete_session,
        )
        self.session.key = b64decode(encryption_key)
        return self.session.run()

    def leave_session(self) -> None:
        self.delete_session()

    def delete_session(self) -> None:
        if self.is_session_host():
            exit_payload = new_exit_payload(
                uid=self.session.uid,
                passphrase=self.session.passphrase,
                host=self.session.host,
                sender=self.session.me,
            )
            self.send_payload(payload=exit_payload, exit_payload=True)
        if self.session:
            self.session.stop()
        self.session = None
        self.last_selection = {}
        self.selection_set = {}

    def enabled_for_window(self, window: sublime.Window) -> bool:
        if window and self.session and self.session.workspace:
            return self.session.workspace.is_workspace(window.id())
        return False

    def enabled_for_view(self, view: sublime.View) -> bool:
        if (
            self.session
            and not is_transient_view(view)
            and self.enabled_for_window(view.window())
            and self.has_write_permissions()
        ):
            return True
        return True

    def send_payload(self, payload: dict, exit_payload: bool = False) -> None:
        self.session.wsm.send_payload(payload=payload, exit_payload=exit_payload)

    def add_view_to_session(self, view, uid: str = '') -> Optional[sublime.View]:
        if not self.session:
            return

        if not uid:
            uid = new_uuid()
        return self.session.workspace.add_view(view, uid)

    def remove_view_from_session(self, uid: str) -> None:
        self.session.workspace.remove_view(uid)

    def is_view_in_session(self, view: sublime.View) -> bool:
        return self.session.workspace.view_exists(view)

    def session_exists(self) -> bool:
        return bool(self.session)

    def get_session(self) -> LiveSession:
        return self.session

    def get_session_info(self) -> List[List]:
        prefix_title = 'Session Identifier, Passphrase'
        info = []
        info.append(
            [
                f'{prefix_title} and Key' if self.session_key() else f'{prefix_title}',
                f'{self.session_id()}:{self.session_passphrase()}:{self.session_key()}'
                if self.session_key()
                else f'{self.session_id()}:{self.session_passphrase()}',
                'Copy to clipboard',
            ]
        )
        info.append(
            ['Active Clients', f'{len(self.session.clients)}', 'Copy to clipboard']
        )
        return info

    def session_id(self) -> str:
        return self.session.uid

    def session_host(self) -> User:
        if not self.session:
            return None
        return self.session.host

    def session_passphrase(self) -> str:
        if not self.session:
            return ''
        return self.session.passphrase

    def session_key(self) -> str:
        if not self.session:
            return ''
        if self.session.key:
            return b64encode(self.session.key).decode('UTF-8')
        return

    def me(self) -> User:
        if not self.session or not self.session.me:
            return False
        return self.session.me

    def is_session_host(self) -> bool:
        if not self.session or not self.session.host:
            return False
        return self.session._is_host()

    def has_write_permissions(self) -> bool:
        if not self.session:
            return bool(self.session)
        return self.session._has_write_permissions()

    def list_active_clients(self):
        clients = [
            [
                client.display_name,
                f'UID: {client.uid}',
                f'Host: {client.host}\tEditor: {client.editor}',
            ]
            for client in self.session.clients
        ]
        self.window.show_quick_panel(
            clients,
            lambda index: _client_actions(
                self.window, self.session, index, self.session.clients
            ),
        )

    def kick_client(self, client_uid) -> None:
        client = next((c for c in self.session.clients if c.uid == client_uid), None)
        if not client:
            return
        _handle_client_action(self.session, 1, client)


session_manager = SessionManager()


def _client_actions(window, session, index: int, clients: List[User]) -> None:
    if index == -1:
        return
    client = clients[index]
    window.show_quick_panel(
        [['Follow', client.display_name], ['Kick', client.display_name]],
        lambda index: _handle_client_action(session, index, client),
    )


def _handle_client_action(session, index, client: User) -> None:
    if index == -1:
        return
    elif index == 0:
        session.following = client
    elif index == 1:
        if not session.me.host:
            return
        payload = new_client_kick_payload(
            session.uid, session.passphrase, session.host, session.host, client
        )
        session.wsm.send_payload(payload)
