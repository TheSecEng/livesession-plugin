import random

from .settings import settings


class User:
    def __init__(self, uid: str, display_name: str, host: bool, editor: bool):
        self.uid = uid
        self.display_name = display_name
        self.host = host
        self.editor = editor
        self.tag_color = random.choice(settings.tag_variables)
        self.tag = None

    def indicator(self) -> str:
        if not self.tag:
            stylesheet = settings.tag_css
            self.tag = (
                f'''<style>{stylesheet}</style>'''
                f'''<div class='user-indicator tag {self.tag_color}'>'''
                f'''<div class='display_name'>{self.display_name}</div></div>'''
            )
        return self.tag
