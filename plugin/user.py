import sublime

from .core.registry import LiveSessionTextCommand


class LiveSessionUnfollowCommand(LiveSessionTextCommand):
    def run(self, _: sublime.Edit) -> None:
        raise NotImplementedError

    def is_visible(self):
        return False

    def is_enabled(self):
        return False


class LiveSessionKickClientCommand(LiveSessionTextCommand):
    def run(self, _: sublime.Edit, client_uid: None) -> None:
        if not client_uid:
            return

        if not self.manager.session_exists():
            return

        self.manager.kick_client(client_uid)
