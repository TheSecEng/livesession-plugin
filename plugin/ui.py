import sublime

from .core.registry import LiveSessionTextCommand


class LiveSessionOpenSessionInfoCommand(LiveSessionTextCommand):
    def run(self, _: sublime.Edit):
        if not self.manager.session_exists():
            return
        self.manager.session.ui_manager.open_or_update_ui()
