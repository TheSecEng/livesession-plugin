import sublime

from .core.typing import List, Tuple
from .core.registry import LiveSessionTextCommand
from .core.manager import session_manager as sm


class LiveSessionApplyDocumentEditCommand(LiveSessionTextCommand):
    def run(self, edit, changes, viewport: Tuple[int, int], following: bool) -> None:
        if not changes:
            return

        read_only = self.view.is_read_only()
        self.view.set_read_only(False)
        for change in changes:
            region = sublime.Region(change['a'], change['b'])
            self._apply_change(region, change['content'], edit)

        self.view.set_read_only(read_only)
        if viewport and following:
            self.view.show(sublime.Region(*viewport))

    def _apply_change(self, region: sublime.Region, replacement: str, edit) -> None:
        if region.empty():
            self.view.insert(edit, region.a, replacement)
        else:
            if len(replacement) > 0:
                self.view.replace(edit, region, replacement)
            else:
                self.view.erase(edit, region)


class LiveSessionSelectionClearCommand(LiveSessionTextCommand):
    def run(self, _: sublime.Edit) -> None:
        self.view.sel().clear()


class LiveSessionSelectionAddCommand(LiveSessionTextCommand):
    def run(
        self,
        _: sublime.Edit,
        user_indicator: str,
        regions: List[Tuple[int, int]],
        viewport: Tuple[int, int],
        following: bool,
    ) -> None:
        if following:
            for region in regions:
                self.view.sel().add(sublime.Region(*region))

            if viewport:
                self.view.show(sublime.Region(*viewport))

        sm.session.update_user_indicator_for_view(
            view=self.view, user_indicator=user_indicator, regions=regions
        )
